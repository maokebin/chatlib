//
//  ChatViewController.h
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"
@interface ChatViewController : UIViewController
@property (retain, nonatomic) IBOutlet UIView *toolView;
@property (retain, nonatomic) IBOutlet HPGrowingTextView *inputTextView;
@property (retain, nonatomic) IBOutlet UITableView *chatTableView;
-(void)initLocalData:(ChatListModel *)model block:(void (^)(BOOL finished))block;
@end
