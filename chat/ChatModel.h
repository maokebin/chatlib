//
//  ChatModel.h
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatModel : NSObject
@property(nonatomic,retain)NSString *userId;
@property(nonatomic,retain)NSString *UserHeadUrl;
@property(nonatomic,retain)NSString *contentMsg;
@property(nonatomic,retain)NSString *sendType;
@property(nonatomic,retain)NSString *sendTime;
@property(nonatomic,retain)NSString *chatId;
@property(nonatomic,retain)NSString *nickName;

-(ChatModel *)initWithDic:(NSDictionary *)dic;
@end
