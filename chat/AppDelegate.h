//
//  AppDelegate.h
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
