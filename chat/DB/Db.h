//
//  Db.h
//  JhonSell
//
//  Created by Mario Montoya on 7/01/09.
//  Copyright 2009 El malabarista. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMResultSet+Additions.h"

@interface Db : NSObject {
	NSString *path;
	FMDatabase* theDb;
    dispatch_queue_t dbqueue;
	BOOL isOpen;
}

@property (retain, nonatomic) FMDatabase *theDb;
@property (retain, nonatomic) NSString *path;
@property (nonatomic) BOOL isOpen;

+(BOOL) dbIsSet;
+(Db *)currentDb;
+(Db *)currentDb: (NSString *)name;

-(void) beginTransaction;
-(void) rollbackTransaction;
-(void) commitTransaction;
-(BOOL) inTransaction;

-(void) createDb;
-(void) openDb;
-(void) closeDb;
-(BOOL) existDb;
+(BOOL) existDb:(NSString *)dbPath;

-(void) checkError;

-(FMResultSet *) load: (NSString *)sql;
-(NSArray *) loadAsDictArray: (NSString *)sql;
-(NSArray *) loadAsDictArrayForMQ: (NSString *)sql;

-(BOOL)execute:(NSString *)sql,...;
-(void)execute:(NSString *)sql array:(NSArray*)bindList;

-(NSInteger) lastRowId;
-(NSInteger) count: (NSString *)tableName;
-(NSInteger) intValue: (NSString *)sql;
-(NSNumber *) numericValue: (NSString *)sql;
-(NSDecimalNumber *) decimalValue: (NSString *)sql;

- (id)initWithName:(NSString *)name;

#pragma mark Pragma functions
-(NSInteger) userDatabaseVersion;

@end
