//
//  Db.m
//  JhonSell
//
//  Created by Mario Montoya on 7/01/09.
//  Copyright 2009 El malabarista. All rights reserved.
//

#import "Db.h"
#import "DbPool.h"

@implementation Db

@synthesize path, theDb,isOpen;

- (id)init {
    if ((self = [super init])) {
        self.path = @"";
        dbqueue = dispatch_queue_create("dbqueque", nil);
		isOpen = NO;
    }
	
	return self;
}

- (id)initWithName:(NSString *)name{
    if ((self = [super init])) {
		self.path = [name copy];
        dbqueue = dispatch_queue_create("dbqueque", nil);
		isOpen = NO;
    }
	
	return self;
}

- (void)dealloc
{
    if (dbqueue) {
        dispatch_release(dbqueue);
    }
    
	[self closeDb];
	
	[theDb release];
	[path release];
	
    [super dealloc];
}

+(BOOL) dbIsSet {
	return [[DbPool sharedInstance] existConn:@"default"];
}

+(id)currentDb {
	return [self currentDb:@"default"];
}

+(id)currentDb: (NSString *)name {
	return [[DbPool sharedInstance] getConn: name];
}

#pragma mark DbAdmin
-(void) createDb{
	if ([self existDb]) {
        return;
		NSException *e = [NSException						  
						  exceptionWithName:@"DBError"						  
						  reason:[NSString stringWithFormat:NSLocalizedString(@"The database file %@ already exist.",@"Error Db: Database file exist"),self.path]
						  userInfo:nil];
		@throw e;
	}
	
	self.theDb = [FMDatabase databaseWithPath: self.path];

	if (![self.theDb open]) {
		NSException *e = [NSException						  
						  exceptionWithName:@"DBError"						  
						  reason:NSLocalizedString(@"Can't create database.",@"Error Db: Database can't be created")
						  userInfo:nil];
		@throw e;
	}
	[self.theDb close];
}

-(void) openDb{
	if (!isOpen) {
		if (![self existDb]) {
            [self createDb];
            return;
			NSString *msg = [NSString stringWithFormat:NSLocalizedString(@"Database file %@ not exist.",@"Error Db: File not exist"),self.path];
			NSException *e = [NSException						  
							  exceptionWithName:@"DBError"						  
							  reason:msg				  
							  userInfo:nil];
			@throw e;			
		}

		self.theDb = [FMDatabase databaseWithPath: self.path];

		if (![self.theDb open]) {
			[self checkError];
			NSException *e = [NSException						  
							  exceptionWithName:@"DBError"						  
							  reason:NSLocalizedString(@"Can't open database. Unknow reason",@"Error Db: Database can't be open")
							  userInfo:nil];
			@throw e;
		}
		
		isOpen = YES;
		//Some optimization tweaks
		[self execute:@"PRAGMA cache_size=100"];
		[self execute:@"PRAGMA foreign_keys = ON"];

		NSLog(@"Db: %@",self.path);
	}
}

-(void) closeDb{
	if (isOpen) {
		[self.theDb close];
		isOpen = NO;
	}
}

-(BOOL) existDb {
	return [Db existDb:self.path];
}

+(BOOL) existDb:(NSString *)dbPath {
	if (dbPath) {
		//
	} else {
		NSException *e = [NSException						  
						  exceptionWithName:@"DBError"						  
						  reason:NSLocalizedString(@"Bad data type.",@"Error Db: Database can't be created")
						  userInfo:nil];
		@throw e;			
	}
	
	return [[NSFileManager defaultManager] fileExistsAtPath: dbPath];
}

-(void) checkError{
    if ([self.theDb hadError]) {
		NSLog(@"Err %d: %@", [self.theDb lastErrorCode], [self.theDb lastErrorMessage]);
		NSException *e = [NSException						  
						  exceptionWithName:@"DBError"						  
						  reason:[self.theDb lastErrorMessage]						  
						  userInfo:nil];
		
		@throw e;
    }
}

#pragma mark Transactions
-(void) beginTransaction {
	if ([self inTransaction]==NO) {
		[self.theDb beginTransaction];
	}
}

-(void) rollbackTransaction {
	if ([self inTransaction]) {
		[self.theDb rollback];
	}
}

-(void) commitTransaction {
	if ([self inTransaction]) {
		[self.theDb commit];
	}
}

-(BOOL) inTransaction {
	[self openDb];
	return [self.theDb inTransaction];
}

#pragma mark DbCommands
-(NSArray *) loadAsDictArray: (NSString *)sql {
	NSMutableArray *list = [[NSMutableArray alloc] init];
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	id value;

	FMResultSet *rs = [self load:sql];
	
	while ([rs next]) {
		NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
		
		for (NSString *fieldName in [rs columnsName]) {
			value = [rs stringForColumn:fieldName];
			if (value) {
				[dict setObject:value forKey:fieldName];
			} else {
				[dict setObject:@"" forKey:fieldName];
			}
		}
		
		[list addObject :dict];
		
		[dict release];
	}
	[rs close];
	[pool drain];
	
	return [list autorelease];
}

-(NSArray *) loadAsDictArrayForMQ: (NSString *)sql
{
    NSMutableArray *list = [[NSMutableArray alloc] init];
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	id value;
    
	FMResultSet *rs = [self load:sql];
	
	while ([rs next]) {
		for (NSString *fieldName in [rs columnsName]) {
            if (![fieldName isEqualToString:@"content"]) {
                continue;
            }
            value = [[rs dataForColumn:fieldName] objectFromJSONData];
			if (value) {
                [list addObject:value];
            }
		}
	}
	[rs close];
	[pool drain];
	
	return [list autorelease];
}


-(FMResultSet *) load: (NSString *)sql{
	[self openDb];
	FMResultSet *rs;
	NSLog(@"%@",sql);
	rs = [self.theDb executeQuery :sql];
	
	[self checkError];
	
	return rs;
}

-(BOOL)execute: (NSString *)sql,...{
	[self openDb];
    va_list args;
    va_start(args, sql);
    BOOL isOk = [self.theDb executeUpdate:sql error:nil withArgumentsInArray:nil orVAList:args];
    va_end(args);
	[self checkError];
    NSLog(@"%@  result = %d",sql,isOk);
    return isOk;
}

-(void)execute:(NSString *)sql array:(NSArray*)bindList
{
    dispatch_async(dbqueue, ^{
        [self beginTransaction];
        BOOL isOk = YES;
        @try {
            for (id obj in bindList) {
                if (![self execute:sql,obj]) {
                    isOk = NO;
                }
            }
            if (isOk == NO) {
                [self rollbackTransaction];
            }
        }
        @catch (NSException * e) {
            [self rollbackTransaction];
            
            @throw e;
        }
        @finally {
            [self commitTransaction];
        }
    });
}


-(NSInteger) lastRowId {
	[self openDb];
	
	NSInteger rowId;

	rowId = [self.theDb lastInsertRowId];
	
	[self checkError];
	
	return rowId;
}

-(NSInteger) count: (NSString *)tableName{
	[self openDb];
	
	NSInteger count;
	
	count = [self.theDb intForQuery:[NSString stringWithFormat: @"SELECT count(1) FROM [%@]",tableName]];
	
	[self checkError];
	
	return count;
}

-(NSInteger) intValue: (NSString *)sql {
	[self openDb];
	
	NSInteger count;
	
	count = [self.theDb intForQuery:sql];
	
	[self checkError];
	
	return count;	
}

-(NSNumber *) numericValue: (NSString *)sql {
	[self openDb];
	NSNumber *value;
	
	value = [NSNumber numberWithDouble:[self.theDb doubleForQuery:sql]];
	
	[self checkError];
	
	return value;
}

-(NSDecimalNumber *) decimalValue: (NSString *)sql {
	NSNumber *value = [self numericValue:sql];

	return [NSDecimalNumber decimalNumberWithDecimal:[value decimalValue]];;
}

#pragma mark Pragma functions
-(NSInteger) userDatabaseVersion {
	return [[self numericValue:@"PRAGMA user_version"] intValue];
}


@end
