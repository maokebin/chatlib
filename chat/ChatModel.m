//
//  ChatModel.m
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "ChatModel.h"

@implementation ChatModel
@synthesize userId;
@synthesize contentMsg;
@synthesize sendTime;
@synthesize sendType;
@synthesize UserHeadUrl;
@synthesize chatId;
@synthesize nickName;
-(ChatModel *)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)dealloc
{
    [userId release];
    [contentMsg release];
    [sendType release];
    [sendTime release];
    [UserHeadUrl release];
    [chatId release];
    [nickName release];
    [super dealloc];
}
@end
