//
//  AddUserViewController.h
//  chat
//
//  Created by 10.8.5 on 14-4-11.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddUserViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITextField *nickName;
@property (retain, nonatomic) IBOutlet UITextField *userId;
@property (retain, nonatomic) IBOutlet UITextField *message;

@end
