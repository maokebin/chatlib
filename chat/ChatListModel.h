//
//  ChatListModel.h
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatListModel : NSObject
@property(nonatomic,retain)NSString *userId;
@property(nonatomic,retain)NSString *headUrl;
@property(nonatomic,retain)NSString *nickName;
@property(nonatomic,retain)NSString *contentMsg;
@property(nonatomic,retain)NSString   *time;
-(ChatListModel *)initWithDic:(NSDictionary *)dic;
@end
