//
//  ChatViewController.m
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "ChatViewController.h"
#import <QuartzCore/QuartzCore.h>
@interface ChatViewController ()<HPGrowingTextViewDelegate,UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    CGRect tableDefaultFrame;
    BOOL isFirstInit;
}

@property(nonatomic,retain)NSMutableArray *chatArray;
@property(nonatomic,retain)ChatListModel *chatUser;
@property(nonatomic,retain)UITapGestureRecognizer *touchTap;
@end

@implementation ChatViewController
@synthesize chatArray;
@synthesize chatUser;
@synthesize touchTap;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.chatArray = [NSMutableArray arrayWithCapacity:10];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.inputTextView.layer.cornerRadius = 5.0;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
    [self loadGrowTextView];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!isFirstInit) {
        isFirstInit = YES;
        tableDefaultFrame = self.chatTableView.frame;
        [self performSelector:@selector(scrollToBottom:) withObject:nil afterDelay:0.1];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self updateListDb];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.inputTextView resignFirstResponder];
}

-(void)initLocalData:(ChatListModel *)model block:(void (^)(BOOL finished))block
{
    self.chatUser = model;
    [[DBManager shareDBManager]getHistoryChatArrayByUserId:model.userId From:nil block:^(NSArray *array) {
        [self.chatArray addObjectsFromArray:array];
        if (block) {
            block(YES);
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [self setChatArray:nil];
    [self setChatUser:nil];
    [self setTouchTap:nil];
    [_toolView release];
    [_inputTextView release];
    [_chatTableView release];
    [super dealloc];
}

-(IBAction)sendAction:(id)sender
{
    ChatModel *Lastmodel = [self.chatArray lastObject];
    NSInteger countid = Lastmodel.chatId.integerValue;
    countid ++;
    ChatModel *model = [[ChatModel alloc]init];
    model.userId = self.chatUser.userId;
    model.sendTime = NSStringFormDouble([[NSDate date]timeIntervalSinceReferenceDate]);
    model.contentMsg = self.inputTextView.text;
    model.chatId = NSStringFormDouble(countid);
    model.sendType = NSStringFormDouble(arc4random()%2);
    [[DBManager shareDBManager]insertChatMessageToDB:@[model]];
    [model release];
    [self.chatArray addObject:model];
    [self insertNewRowToChatTable];
    [self scrollToBottom:YES];
}

-(void)insertNewRowToChatTable
{
    NSIndexPath *path = [NSIndexPath indexPathForRow:self.chatArray.count - 1 inSection:0];
    [self.chatTableView insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
}



-(void)updateListDb
{
    self.chatUser.time = ((ChatModel *)[self.chatArray lastObject]).sendTime;
    self.chatUser.contentMsg = ((ChatModel *)[self.chatArray lastObject]).contentMsg;
    [[DBManager shareDBManager]insertOneListCellToDB:self.chatUser];
}

-(void)loadGrowTextView
{
    self.inputTextView.isScrollable = NO;
    self.inputTextView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
	self.inputTextView.minNumberOfLines = 1;
	self.inputTextView.maxNumberOfLines = 6;
    // you can also set the maximum height in points with maxHeight
    // textView.maxHeight = 200.0f;
	self.inputTextView.returnKeyType = UIReturnKeyDone; //just as an example
	self.inputTextView.font = [UIFont systemFontOfSize:15.0f];
	self.inputTextView.delegate = self;
    self.inputTextView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.inputTextView.backgroundColor = [UIColor whiteColor];
    self.inputTextView.placeholder = @"请输入";
}

-(void)resignTextView
{
	[self.inputTextView resignFirstResponder];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = self.toolView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    CGRect tableFrame = tableDefaultFrame;
    if (self.chatTableView.contentSize.height < tableFrame.size.height) {
        tableFrame.size.height = containerFrame.origin.y;
    }else{
        tableFrame.origin.y = -keyboardBounds.size.height;
    }
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
	// set views with new info
	self.toolView.frame = containerFrame;
    self.chatTableView.frame = tableFrame;
	// commit animations
	[UIView commitAnimations];
    [self.chatTableView setUserInteractionEnabled:NO];
    [self scrollToBottom:YES];
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = self.toolView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	self.toolView.frame = containerFrame;
	self.chatTableView.frame = tableDefaultFrame;
	// commit animations
	[UIView commitAnimations];
    [self.chatTableView setUserInteractionEnabled:YES];
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
	CGRect r = self.toolView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	self.toolView.frame = r;
}

- (BOOL)growingTextViewShouldReturn:(HPGrowingTextView *)growingTextView;
{
    [self performSelector:@selector(sendAction:) withObject:nil afterDelay:0.25 inModes:@[NSRunLoopCommonModes]];
    return YES;
}

-(void)scrollToBottom:(BOOL)animated
{
    if (self.chatArray.count) {
        [self.chatTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:MAX(0, self.chatArray.count -1) inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:animated];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.chatArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"test1"];
    if (!cell) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"test1"]autorelease];
    }
    ChatModel *model = [self.chatArray objectAtIndex:indexPath.row];
    cell.textLabel.text = model.contentMsg;
    return cell;
}
@end
