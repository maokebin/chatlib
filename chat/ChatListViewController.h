//
//  ChatListViewController.h
//  chat
//
//  Created by 10.8.5 on 14-4-11.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListViewController : UIViewController
@property (retain, nonatomic) IBOutlet UITableView *listTable;

@end
