//
//  ChatListModel.m
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "ChatListModel.h"

@implementation ChatListModel
@synthesize userId;
@synthesize headUrl;
@synthesize nickName;
@synthesize contentMsg;
@synthesize time;

-(ChatListModel *)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

-(void)dealloc
{
    [userId release];
    [headUrl release];
    [nickName release];
    [contentMsg release];
    [time release];
    [super dealloc];
}
@end
