//
//  AddUserViewController.m
//  chat
//
//  Created by 10.8.5 on 14-4-11.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "AddUserViewController.h"

@interface AddUserViewController ()

@end

@implementation AddUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_nickName release];
    [_userId release];
    [_message release];
    [super dealloc];
}

-(IBAction)addUserAction:(id)sender
{
    if (!self.nickName.text.length || !self.message.text.length || !self.userId.text.length) {
        return;
    }
    ChatListModel *model = [[[ChatListModel alloc]init]autorelease];
    model.nickName = self.nickName.text;
    model.contentMsg = self.message.text;
    model.userId = self.userId.text;
    model.time   = NSStringFormDouble([[NSDate date]timeIntervalSince1970]);
    [[DBManager shareDBManager]insertOneListCellToDB:model];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
