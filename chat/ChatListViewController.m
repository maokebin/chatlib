//
//  ChatListViewController.m
//  chat
//
//  Created by 10.8.5 on 14-4-11.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatViewController.h"
#import "AddUserViewController.h"
@interface ChatListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property(nonatomic,retain)NSMutableArray *listArray;
@property(nonatomic,retain)NSIndexPath *currentIndex;
@end

@implementation ChatListViewController
@synthesize listArray;
@synthesize currentIndex;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.listArray = [NSMutableArray arrayWithCapacity:10];
    self.listTable.delegate = self;
    self.listTable.dataSource = self;
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"add" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(addUserAction:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, 45, 45);
    button.backgroundColor = [UIColor blackColor];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem =[[[UIBarButtonItem alloc]initWithCustomView:button]autorelease];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self loadLocalData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_listTable release];
    [self setCurrentIndex:nil];
    [super dealloc];
}
-(void)addUserAction:(id)sender
{
    AddUserViewController *add = [[[AddUserViewController alloc]initWithNibName:nil bundle:nil]autorelease];
    [self.navigationController pushViewController:add animated:YES];
}

-(void)loadLocalData
{
    [[DBManager shareDBManager]getHistoryChatListArray:[NSDate date] block:^(NSArray *array) {
        [self.listArray removeAllObjects];
        [self.listArray addObjectsFromArray:array];
        [self.listTable reloadData];
    }];
}

-(void)updateListIndex
{
    [self.listArray exchangeObjectAtIndex:self.currentIndex.row withObjectAtIndex:0];
    [self.listTable moveRowAtIndexPath:self.currentIndex toIndexPath:[NSIndexPath indexPathForRow:self.currentIndex.row inSection:self.currentIndex.section]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return self.listArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"chatList"];
    if (!cell) {
        cell = [[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"chatList"]autorelease];
    }
    ChatListModel *model = [self.listArray objectAtIndex:indexPath.row];
    cell.textLabel.text = model.nickName;
    cell.detailTextLabel.text = model.contentMsg;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.view.userInteractionEnabled = NO;
    self.currentIndex = indexPath;
    ChatListModel *model = [self.listArray objectAtIndex:indexPath.row];
    ChatViewController *chat = [[ChatViewController alloc]initWithNibName:nil bundle:nil];
    [chat initLocalData:model block:^(BOOL finished) {
        self.view.userInteractionEnabled = YES;
        [self.navigationController pushViewController:chat animated:YES];
    }];
    [chat release];
}

@end
