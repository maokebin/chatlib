//
//  DBManager.m
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import "DBManager.h"
#import "ChatModel.h"
#define CACHE_PATH     [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)objectAtIndex:0]
#define data_base_path [CACHE_PATH stringByAppendingPathComponent:@"database.sqlite"]

#define TableNameByUserId(UserId) ({ \
    NSString *tableName = [NSString stringWithFormat:@"User_%@",UserId];\
    tableName;\
})

static dispatch_queue_t manageDbQueue;
static dispatch_queue_t getManageDbQueue() {
    if (manageDbQueue == NULL) {
        manageDbQueue = dispatch_queue_create("manageDbQueue", 0);
    }
    return manageDbQueue;
}
#define Dispatch_Main_Begin     dispatch_sync(dispatch_get_main_queue(), ^{
#define Dispatch_Main_End           });

#define Dispatch_Begin     @synchronized(self){\
dispatch_async(getManageDbQueue(), ^{

#define Dispatch_end                });\
}
@interface DBManager()

@end
@implementation DBManager
@synthesize database;

+(DBManager*)shareDBManager
{
    static DBManager *db = nil;
    if (!db) {
        db = [[DBManager alloc]init];
    }
    return db;

}

-(id)init
{
    self = [super init];
    if (self) {
        [self openAndInitDataBase];
    }
    return self;
}

-(void)openAndInitDataBase
{
    self.database = [[[Db alloc]initWithName:data_base_path]autorelease];
    [self checkAndCreateListTable];
}


-(BOOL)checkAndCreateListTable
{
    if (![self.database execute:@"CREATE TABLE IF NOT EXISTS ChatList(_id integer primary key autoincrement, userId text,headUrl text,nickName text,msgContent text,time text)"])
    {
        return NO;
    }else{
        return YES;
    }
}


-(BOOL)checkAndCreateChatTable:(NSString *)userId
{
    NSString *sql = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@(_id integer primary key autoincrement, userId text,headUrl text,nickName text,msgContent text,time text,chatId text,sendType text)",TableNameByUserId(userId)];
    if (![self.database execute:sql])
    {
        NSLog(@"can't create Usertable %@",userId);
        return NO;
    }
    return YES;
}

-(void)insertOneListCellToDB:(ChatListModel *)model
{
    Dispatch_Begin
    if (model ==nil) {
        return;
    }
    if (![self checkAndCreateListTable]) {
        return;
    }
    // insert into .... on duplicate key update
    NSString *sql  = [NSString stringWithFormat:@"SELECT * FROM ChatList where (userId == %@)",model.userId];
    FMResultSet *rs = [self.database load:sql];
    if ([rs next]) {
        NSString *update  = [NSString stringWithFormat:@"UPDATE  ChatList SET headUrl=? ,nickName = ?, msgContent = ?,time = ? WHERE userId=?"];
        [self.database execute:update,model.headUrl,model.nickName,model.contentMsg,model.time,model.userId];
    }else{
        NSString *insert  = [NSString stringWithFormat:@"insert into ChatList(userId,headUrl,nickName,msgContent,time) VALUES(?,?,?,?,?)"];
        [self.database execute:insert,model.userId,model.headUrl,model.nickName,model.contentMsg,model.time];
    }
    Dispatch_end
}

-(void)insertOneChatMessageToDB:(ChatModel *)model
{
    if (model ==nil) {
        return;
    }
    Dispatch_Begin
    if (![self checkAndCreateChatTable:model.userId]) {
        return;
    }
    
    NSString *sql  = [NSString stringWithFormat:@"SELECT * FROM %@ where (chatId == %@)",TableNameByUserId(model.userId),model.chatId];
    FMResultSet *rs = [self.database load:sql];
    if ([rs next]) {
        return;
    }
    NSString *insert  = [NSString stringWithFormat:@"INSERT INTO %@(userId,headUrl,nickName,msgContent,time,chatId,sendType)  VALUES(?,?,?,?,?,?) ",model.userId];
    [self.database execute:insert,model.userId,model.UserHeadUrl,model.nickName,model.contentMsg,model.sendTime,model.chatId,model.sendType];
    Dispatch_end
}

-(void)insertChatMessageToDB:(NSArray *)modelArray
{
    if (modelArray ==nil) {
        return;
    }
    Dispatch_Begin
    if (modelArray.count == 0) {
        return ;
    }
    ChatModel *model = [modelArray lastObject];
    if (![self checkAndCreateChatTable:model.userId]) {
        return;
    }
    for (ChatModel *model in modelArray) {
        NSString *sql  = [NSString stringWithFormat:@"SELECT * FROM %@ where (chatId == %@)",TableNameByUserId(model.userId),model.chatId];
        FMResultSet *rs = [self.database load:sql];
        if ([rs next]) {
            continue;
        }
        NSString *insert  = [NSString stringWithFormat:@"INSERT INTO %@(userId,headUrl,nickName,msgContent,time,chatId,sendType)  VALUES(?,?,?,?,?,?,?)",TableNameByUserId(model.userId)];
       [self.database execute:insert,model.userId,model.UserHeadUrl,model.nickName,model.contentMsg,model.sendTime,model.chatId,model.sendType];
    }
    Dispatch_end
}

-(void)getHistoryChatArrayByUserId:(NSString *)userId From:(NSString *)chatId block:(void (^)(NSArray *array))block
{
    Dispatch_Begin
    [self checkAndCreateChatTable:userId];
    NSString *sql = nil;
    if (!chatId || chatId.integerValue == 0) {
        sql  = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY chatId ASC LIMIT 0 , 30",TableNameByUserId(userId)];
    }else{
        sql  = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE(chatId < %@) ORDER BY chatId DESC LIMIT 0 , 30",TableNameByUserId(userId),chatId];
    }
    FMResultSet *rs = [self.database load:sql];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
    while (([rs next])) {
        ChatModel *chat = [[[ChatModel alloc]init]autorelease];
        chat.nickName = [rs stringForColumn:@"nickName"];
        chat.userId = [rs stringForColumn:@"userId"];
        chat.UserHeadUrl = [rs stringForColumn:@"headUrl"];
        chat.contentMsg = [rs stringForColumn:@"msgContent"];
        chat.sendTime = [rs stringForColumn:@"time"];
        chat.chatId  = [rs stringForColumn:@"chatId"];
        chat.sendType = [rs stringForColumn:@"sendType"];
        [array addObject:chat];
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (block) {
            block(array);
        }
    });
    Dispatch_end
}

-(void)getHistoryChatListArray:(NSDate *)Beforetime block:(void (^)(NSArray *array))block
{
    Dispatch_Begin
    NSString *sql = nil;
    sql  = [NSString stringWithFormat:@"SELECT * FROM ChatList ORDER BY time DESC"];
    FMResultSet *rs = [self.database load:sql];
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:10];
    while (([rs next])) {
        ChatListModel *chat = [[[ChatListModel alloc]init]autorelease];
        chat.nickName = [rs stringForColumn:@"nickName"];
        chat.userId = [rs stringForColumn:@"userId"];
        chat.headUrl = [rs stringForColumn:@"headUrl"];
        chat.contentMsg = [rs stringForColumn:@"msgContent"];
        chat.time = [rs stringForColumn:@"time"];
        [array addObject:chat];
    }
    dispatch_sync(dispatch_get_main_queue(), ^{
        if (block) {
            block(array);
        }
    });
    Dispatch_end
}

@end
