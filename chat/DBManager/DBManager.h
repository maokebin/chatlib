//
//  DBManager.h
//  chat
//
//  Created by chris on 14-4-8.
//  Copyright (c) 2014年 chris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Db.h"
#import "ChatListModel.h"
#import "ChatModel.h"
@interface DBManager : NSObject
@property(nonatomic,retain)Db *database;
+(DBManager*)shareDBManager;
-(void)insertOneListCellToDB:(ChatListModel *)model;
-(void)insertOneChatMessageToDB:(ChatModel *)model;
-(void)insertChatMessageToDB:(NSArray *)modelArray;
-(void)getHistoryChatArrayByUserId:(NSString *)userId From:(NSString *)chatId block:(void (^)(NSArray *array))block;
-(void)getHistoryChatListArray:(NSDate *)Beforetime block:(void (^)(NSArray *array))block;

@end
